# ImageUtils

[![CI Status](https://img.shields.io/travis/jeka-mel/ImageUtils.svg?style=flat)](https://travis-ci.org/jeka-mel/ImageUtils)
[![Version](https://img.shields.io/cocoapods/v/ImageUtils.svg?style=flat)](https://cocoapods.org/pods/ImageUtils)
[![License](https://img.shields.io/cocoapods/l/ImageUtils.svg?style=flat)](https://cocoapods.org/pods/ImageUtils)
[![Platform](https://img.shields.io/cocoapods/p/ImageUtils.svg?style=flat)](https://cocoapods.org/pods/ImageUtils)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ImageUtils is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ImageUtils'
```

## Author

jeka-mel, iosdeveloper.mail@gmail.com

## License

ImageUtils is available under the MIT license. See the LICENSE file for more info.
